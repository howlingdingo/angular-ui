import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Garden } from 'src/app/model/Garden';
import { GardenService } from 'src/app/service/garden.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  gardens: Array<Garden> = [{name:'asdf', description: 'adsf'}];
  
  displayedColumns: string[] = ['name', 'description' ];

  constructor(private gardenService: GardenService) { }

  ngOnInit(): void {
    this.loadGardens();
  }
  
  loadGardens() {
    this.gardenService.getGardens().subscribe({
      next: gardens => {
        this.gardens = gardens;
      }
    })
  }


  name = new FormControl('', [Validators.required]);
  description = new FormControl('', [Validators.required]);

  onSubmit() {
    console.log(`name: [${this.name.value}]`)
    console.log(`description: [${this.description.value}]`)
    const garden: Garden = { name: this.name.value, description : this.description.value};
    this.gardenService.addGarden(garden).subscribe({
      next: () => {
        alert("Garden created");
        this.name.setValue("");
        this.description.setValue("");
        this.loadGardens();
      },
    })
  }
}
