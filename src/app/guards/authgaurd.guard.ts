import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { MeService } from '../service/me.service';

@Injectable({
  providedIn: 'root'
})
export class AuthgaurdGuard implements CanActivate {
  constructor(
    private router: Router,
    private meService: MeService,
  ) { }
  
  canActivate(
    _route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    return new Observable<boolean>((observer) => {
      this.meService.getUser().toPromise().then(value => {
        if (value) {
          // have retrieved a user!
          observer.next(true);
          observer.complete();
          return;
        }
        this.router.navigate(['/login'], { queryParams: { returnUrl : state.url }});
        observer.next(false);
        observer.complete();
      }).catch(error => {
        // unauthed...
        this.router.navigate(['/login'], { queryParams: { returnUrl : state.url }});
        observer.next(false);
        observer.complete();
      })
    })
  }
}
