import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './gardens/dashboard/dashboard.component';
import { HomeComponent } from './gardens/home/home.component';
import { AuthgaurdGuard } from './guards/authgaurd.guard';
import { LoginComponent } from './login/login/login.component';
import { SignupComponent } from './login/signup/signup.component';
import { WelcomeLayoutComponent } from './login/welcome-layout/welcome-layout.component';

const routes: Routes = [
  { 
    path: 'login',
    component:  WelcomeLayoutComponent,
    children: [
      { path: '', component: LoginComponent}, 
    ]
  },
  { 
    path: 'signup',
    component:  WelcomeLayoutComponent,
    children: [
      { path: '', component:  SignupComponent},
    ]
  },
  { 
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [ AuthgaurdGuard ]
  },
  { 
    path: '**',
    component:
    HomeComponent,
    canActivate: [ AuthgaurdGuard ]
  },
]
  
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
