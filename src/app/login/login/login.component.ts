import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MeService } from 'src/app/service/me.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private meService: MeService, private router: Router
    ) { }


    email = new FormControl('', [Validators.required, Validators.email]);
    password = new FormControl('', [Validators.required]);

  ngOnInit(): void {
  }


  submitted = false;

  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }

    return this.email.hasError('email') ? 'Not a valid email' : '';
  }

  onSubmit() {
    console.log(`email: [${this.email.value}]`)
    console.log(`pw: [${this.password.value}]`)
    const user = this.meService.login(this.email.value, this.password.value);
    user.subscribe({
      next: value => this.router.navigate(['/dashboard']),
      error: error => console.log(error),
    })
  }
}
