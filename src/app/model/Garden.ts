
export class Garden {
    id?: string;
    name?: string;
    ownerId?: string;
    description?: string;
}