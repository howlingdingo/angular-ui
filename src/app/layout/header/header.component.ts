import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MeService } from 'src/app/service/me.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  user: any;

  constructor(
    private meService: MeService, 
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.meService.getUser().subscribe(
      {
        next: (user) => {
          this.user = user;
        },
        error: (_err) => {
          // ignore - we are not authed...
          this.user = undefined;
        }
      }
    )
  }

  logout() {
   this.meService.logout().subscribe(
     {
       next: () => {
         this.router.navigate(['/login'])
       }
     }
   )
  }
}
