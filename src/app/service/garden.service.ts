import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Garden } from '../model/Garden';
@Injectable({
  providedIn: 'root'
})  
export class GardenService {

  constructor(private http: HttpClient) { }

  public getGardens(): Observable<[Garden]> {
    return this.http.get<[Garden]>('/api/v1/gardens/', {withCredentials: true});
  }

  public addGarden(garden: Garden): Observable<[Garden]> {
    return this.http.post<[Garden]>('/api/v1/gardens/', garden, {withCredentials: true});
  }

}
