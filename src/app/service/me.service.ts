import { Injectable } from '@angular/core';
import { User } from '../model/User';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Injectable({
  providedIn: 'root'
})
export class MeService {

  public user?: Observable<User>;

  public currentUser: any;

  constructor(private http: HttpClient,)
  { 
  }

  getUser() {
    return this.http.get('/api/v1/users/me', {withCredentials: true});
  }

  login(email: string, password: string){
    return this.http.post<User>(`/api/v1/auth/login/`, { email, password }, {withCredentials: true});
  };

  logout(){
    return this.http.post<User>(`/api/v1/auth/logout/`, { }, {withCredentials: true});
  };

}
